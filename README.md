# README #

This README presents information about this repository.


### What is this repository for? ###

* This software development practical is part of the UE-GLI module at ISTIC, University of Rennes 1.
* The Observer and Command patterns, in a variant of the MVC (Model-View-Control) design, are used in a
* very simple pie chart application (in French, "camembert", not a pie, but a cheese).
* JavaFX 2.2 is used for the graphics, mouse and keyboard interaction.
* V0.1   : sample code to test JavaFX features.
* V0.2   : Architecture defined in cheese-design.pdf. Data Model tested.
* V0.3   : View scenario tested using simple console output module.
* V0.4   : Redesign DataModel to protect internal mutable data, necessary to avoid View corrupting DataModel!!!
* V0.5   : Create command primitives and Memento Pattern to manage changing DM item values, and undoing those changes
		(NB: CommandTest => algorithms that can be used in Control module below)
* V0.6   : Add persistence to DataModel using serialisation to file. Database tested.
* V0.7   : Build Control module as invoker of commands, DM initialiser, view attacher, DB attacher.
* V0.8   : Creat interaction loop using JavaFX and keyboard input, with ConsoleViewer. Basic MVC is now complete.
* V0.9   : Graphical View scenario added and tested. Multi-view MVC complete.
* V1.0   : Complete system tested.

 ***Current Version: V0.5***


### How do I get set up? ###

* Summary of set up: downlowd delivery packages and run as Java Application in Eclipse.
* Configuration: Eclipse Luna package is provided as TP-Cheese.zip [=TBD=]
* Dependencies: JavaSE-1.8, JUnit 4
* Database configuration: none, a simple text file is used
* How to run tests: JUnit tests are provided


### Contribution guidelines ###

* Read-only for this study work.
* Code review: comments are welcome.


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
