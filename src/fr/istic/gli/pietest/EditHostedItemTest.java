package fr.istic.gli.pietest;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import fr.istic.gli.data.DataModel;
import fr.istic.gli.data.EditDataModelAdd;
import fr.istic.gli.data.EditDataModelLabel;
import fr.istic.gli.data.EditDataModelRemove;
import fr.istic.gli.data.EditRecordContents;
import fr.istic.gli.data.EditRecordLabel;
import fr.istic.gli.data.Record;

/**
 * this class acts as an Invoker to test the individual command methods
 *
 */
public class EditHostedItemTest {

	public static DataModel dm = null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Logger.getGlobal().setLevel(Level.ALL);
		Logger.getGlobal().info("setting up EditHostedItem test class");
		dm = new DataModel("Shared Data Model");
		
		GlobalTestMode.setIsTestMode(true);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Logger.getGlobal().info("closing EditHostedItem test class");
		
		GlobalTestMode.setIsTestMode(false);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	
	/**
	 * Utility function.
	 * @throws Exception
	 */
	public void invariant() throws Exception {
		assertNotEquals("broken invariance", null, dm);
	}
	
	/**
	 * Utility function.
	 * @throws Exception
	 */
	public void confirmDataModelInit() throws Exception {
		final String msgDMinit = "DataModel: name=\"Shared Data Model\", with records: <no records found>";
		assertTrue("Data Model non-initialised", dm.toString().matches(msgDMinit));
	}

	
	@Test
	public void testEditRecordLabel() throws Exception {
		Logger.getGlobal().info("**** Starting testEditRecordLabel()");
		invariant();
		
		Record r1 = new Record("r1", 21.22);
		Record r2 = new Record("r2", 22.22);
		final String assertMsg = " doesn't have the expected label";

		System.out.println(r1.toString());
		System.out.println(r2.toString());
		
		Logger.getGlobal().info("setting up edit commands");
		EditRecordLabel editLabelr1 = new EditRecordLabel(r1, "r1edited");
		EditRecordLabel editLabelr2 = new EditRecordLabel(r2, "r2edited");

		assertTrue("r1" + assertMsg, r1.getLabel().matches("r1"));
		assertTrue("r2" + assertMsg, r2.getLabel().matches("r2"));

		boolean editLabelr1OK = editLabelr1.undoIt();
		boolean editLabelr2OK = editLabelr2.undoIt();
		assertFalse(editLabelr1OK);
		assertFalse(editLabelr2OK);
		assertFalse(editLabelr1.hasBeenDone());
		assertFalse(editLabelr2.hasBeenDone());

		editLabelr1OK = editLabelr1.doIt();
		editLabelr2OK = editLabelr2.doIt();
		assertTrue(editLabelr1OK);
		assertTrue(editLabelr2OK);
		assertTrue(editLabelr1.hasBeenDone());
		assertTrue(editLabelr2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getLabel().matches("r1edited"));
		assertTrue("r2" + assertMsg, r2.getLabel().matches("r2edited"));
		System.out.println("edit operation is " + editLabelr1OK + " ... " + r1.toString());
		System.out.println("edit operation is " + editLabelr2OK + " ... " + r2.toString());
		
		editLabelr1OK = editLabelr1.doIt();
		editLabelr2OK = editLabelr2.doIt();
		assertFalse(editLabelr1OK);
		assertFalse(editLabelr2OK);
		assertTrue(editLabelr1.hasBeenDone());
		assertTrue(editLabelr2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getLabel().matches("r1edited"));
		assertTrue("r2" + assertMsg, r2.getLabel().matches("r2edited"));
		System.out.println("edit operation is " + editLabelr1OK + " ... " + r1.toString());
		System.out.println("edit operation is " + editLabelr2OK + " ... " + r2.toString());

		editLabelr1OK = editLabelr1.undoIt();
		editLabelr2OK = editLabelr2.undoIt();
		System.out.println(dm.toString());
		assertTrue(editLabelr1OK);
		assertTrue(editLabelr2OK);
		assertFalse(editLabelr1.hasBeenDone());
		assertFalse(editLabelr2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getLabel().matches("r1"));
		assertTrue("r2" + assertMsg, r2.getLabel().matches("r2"));
		System.out.println("edit operation is " + editLabelr1OK + " ... " + r1.toString());
		System.out.println("edit operation is " + editLabelr2OK + " ... " + r2.toString());
		
		editLabelr1OK = editLabelr1.undoIt();
		editLabelr2OK = editLabelr2.undoIt();
		System.out.println(dm.toString());
		assertFalse(editLabelr1OK);
		assertFalse(editLabelr2OK);
		assertFalse(editLabelr1.hasBeenDone());
		assertFalse(editLabelr2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getLabel().matches("r1"));
		assertTrue("r2" + assertMsg, r2.getLabel().matches("r2"));
		System.out.println("edit operation is " + editLabelr1OK + " ... " + r1.toString());
		System.out.println("edit operation is " + editLabelr2OK + " ... " + r2.toString());

		invariant();
		
		editLabelr1OK = editLabelr1.doIt();
		editLabelr2OK = editLabelr2.doIt();
		assertTrue(editLabelr1OK);
		assertTrue(editLabelr2OK);
		assertTrue(editLabelr1.hasBeenDone());
		assertTrue(editLabelr2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getLabel().matches("r1edited"));
		assertTrue("r2" + assertMsg, r2.getLabel().matches("r2edited"));
		System.out.println("edit operation is " + editLabelr1OK + " ... " + r1.toString());
		System.out.println("edit operation is " + editLabelr2OK + " ... " + r2.toString());
		
		dm.deleteItem(r1.getKey());
		Logger.getGlobal().info("**** Ending testEditRecordLabel()\n");
	}

	@Test
	public void testEditRecordContents() throws Exception {
		Logger.getGlobal().info("**** Starting testEditRecordContents()");
		invariant();
		
		Record r1 = new Record("r1", 21.22);
		Record r2 = new Record("r2", 22.22);
		final String assertMsg = " doesn't have the expected contents";

		System.out.println(r1.toString());
		System.out.println(r2.toString());
		
		Logger.getGlobal().info("setting up edit commands");
		EditRecordContents erl1 = new EditRecordContents(r1, 91.99);
		EditRecordContents erl2 = new EditRecordContents(r2, 92.99);
		assertTrue("r1" + assertMsg, r1.getContents().equals(21.22));
		assertTrue("r2" + assertMsg, r2.getContents().equals(22.22));

		System.out.println(r1.toString());
		System.out.println(r2.toString());

		boolean erl1OK = erl1.undoIt();
		boolean erl2OK = erl2.undoIt();
		assertFalse(erl1OK);
		assertFalse(erl2OK);
		assertFalse(erl1.hasBeenDone());
		assertFalse(erl2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getContents().equals(21.22));
		assertTrue("r2" + assertMsg, r2.getContents().equals(22.22));

		System.out.println(r1.toString());
		System.out.println(r2.toString());

		erl1OK = erl1.doIt();
		erl2OK = erl2.doIt();
		assertTrue(erl1OK);
		assertTrue(erl2OK);
		assertTrue(erl1.hasBeenDone());
		assertTrue(erl2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getContents().equals(91.99));
		assertTrue("r2" + assertMsg, r2.getContents().equals(92.99));

		System.out.println(r1.toString());
		System.out.println(r2.toString());
		
		erl1OK = erl1.doIt();
		erl2OK = erl2.doIt();
		assertFalse(erl1OK);
		assertFalse(erl2OK);
		assertTrue(erl1.hasBeenDone());
		assertTrue(erl2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getContents().equals(91.99));
		assertTrue("r2" + assertMsg, r2.getContents().equals(92.99));

		System.out.println(r1.toString());
		System.out.println(r2.toString());
		
		erl1OK = erl1.undoIt();
		erl2OK = erl2.undoIt();
		assertTrue(erl1OK);
		assertTrue(erl2OK);
		assertFalse(erl1.hasBeenDone());
		assertFalse(erl2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getContents().equals(21.22));
		assertTrue("r2" + assertMsg, r2.getContents().equals(22.22));

		erl1OK = erl1.undoIt();
		erl2OK = erl2.undoIt();
		assertFalse(erl1OK);
		assertFalse(erl2OK);
		assertFalse(erl1.hasBeenDone());
		assertFalse(erl2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getContents().equals(21.22));
		assertTrue("r2" + assertMsg, r2.getContents().equals(22.22));

		invariant();
		
		erl1OK = erl1.doIt();
		erl2OK = erl2.doIt();
		assertTrue(erl1OK);
		assertTrue(erl2OK);
		assertTrue(erl1.hasBeenDone());
		assertTrue(erl2.hasBeenDone());
		assertTrue("r1" + assertMsg, r1.getContents().equals(91.99));
		assertTrue("r2" + assertMsg, r2.getContents().equals(92.99));

		System.out.println(r1.toString());
		System.out.println(r2.toString());
		
		Logger.getGlobal().info("**** Ending testEditRecordContents()\n");
	}

	@Test
	public void testEditDataModelName() throws Exception {
		Logger.getGlobal().info("**** Starting testEditDataModelName()");
		invariant();
		System.out.println(dm.toString());
		confirmDataModelInit();

		Logger.getGlobal().info("setting up edit commands");
		EditDataModelLabel edml = new EditDataModelLabel(dm, "dm-edited");
		System.out.println(dm.getDataModelName());
		assertTrue("dm label is not as expected", dm.getDataModelName().matches("Shared Data Model"));

		boolean edmlOK = edml.undoIt();
		assertFalse(edmlOK);
		assertFalse(edml.hasBeenDone());

		edmlOK = edml.doIt();
		System.out.println(dm.getDataModelName());
		assertTrue("dm label is not as expected", dm.getDataModelName().matches("dm-edited"));
		assertTrue(edmlOK);
		assertTrue(edml.hasBeenDone());
		
		edmlOK = edml.doIt();
		System.out.println("Edit is still done after a second doIt(): " + dm.getDataModelName());
		assertTrue("dm label is not stable when done again", dm.getDataModelName().matches("dm-edited"));
		assertFalse(edmlOK);
		assertTrue(edml.hasBeenDone());

		edmlOK = edml.undoIt();
		System.out.println(dm.getDataModelName());
		assertTrue("dm label has not been undone correctly", dm.getDataModelName().matches("Shared Data Model"));
		assertTrue(edmlOK);
		assertFalse(edml.hasBeenDone());
		
		edmlOK = edml.undoIt();
		System.out.println(dm.getDataModelName());
		assertTrue("dm label is not stable when undone again", dm.getDataModelName().matches("Shared Data Model"));
		assertFalse(edmlOK);
		assertFalse(edml.hasBeenDone());
		invariant();
		
		edmlOK = edml.doIt();
		System.out.println(dm.toString());
		assertTrue(edmlOK);
		assertTrue(edml.hasBeenDone());
		
		invariant();
		edml.undoIt();
		Logger.getGlobal().info("**** Ending testEditDataModelName()\n");
	}

	@Test
	public void testEditDataModelRecords() throws Exception {
		Logger.getGlobal().info("**** Starting testEditDataModelRecords()");
		invariant();
		confirmDataModelInit();
		System.out.println(dm.toString());

		final String msgIsInCheck = " record key is not in host";
		final String msgIsNotInCheck = " record key should not be in host";
		final String msgAddCheck = " addition has unexpected result";
		final String msgUpCheck = " update has unexpected result";
		final String msgDelCheck = " deletion has unexpected result";
		
		Record r1 = new Record("r1", 21.22);
		Record r2 = new Record("r2", 22.22);
		Record r3 = new Record("r3", 23.22);
		long kr1a = dm.addItem(r1);
		System.out.println(dm.toString());
		System.out.println(r2.toString());
		System.out.println(r3.toString());
		
		Logger.getGlobal().info("setting up edit commands");
		EditDataModelAdd dataAdd1 = new EditDataModelAdd(dm, r1); // test duplication
		EditDataModelAdd dataAdd2 = new EditDataModelAdd(dm, r2);
		EditDataModelRemove dataDel1a = new EditDataModelRemove(dm, kr1a);

		assertTrue("r1a" + msgIsInCheck, dm.readItem(kr1a).equals(r1));
		assertTrue(dm.getKeys().size()==1);

		assertTrue("r1b" + msgIsNotInCheck, dataAdd1.getHostedItem(dm)==null);
		assertTrue("r2" + msgIsNotInCheck, dataAdd2.getHostedItem(dm)==null);

		boolean dataAdd1OK = dataAdd1.undoIt();
		boolean dataAdd2OK = dataAdd2.undoIt();
		assertFalse("r1" + msgAddCheck, dataAdd1OK);
		assertFalse("r2" + msgAddCheck, dataAdd2OK);
		dataAdd1OK = dataAdd1.doIt();
		dataAdd2OK = dataAdd2.doIt();
		assertTrue("r1" + msgAddCheck, dataAdd1OK);
		assertTrue("r2" + msgAddCheck, dataAdd2OK);
		dataAdd1OK = dataAdd1.doIt();
		dataAdd2OK = dataAdd2.doIt();
		assertFalse("r1" + msgAddCheck, dataAdd1OK);
		assertFalse("r2" + msgAddCheck, dataAdd2OK);
		System.out.println("after additions: " + dm.toString());
		
		for (long i : dm.getKeysOrderByContents()) {
			System.out.println("next key=" + i + " holds >>> " + dm.readItem(i));
		}

		LinkedList<Long> lablist = dm.getKeysOrderByLabels();
		assertTrue(lablist.size()==3);
		
		EditDataModelRemove dataDel0 = new EditDataModelRemove(dm, lablist.get(0));
		EditDataModelRemove dataDel1 = new EditDataModelRemove(dm, lablist.get(1));
		EditDataModelRemove dataDel2 = new EditDataModelRemove(dm, lablist.get(2));
		boolean dataDel0OK = dataDel0.undoIt();
		boolean dataDel1OK = dataDel1.undoIt();
		boolean dataDel2OK = dataDel2.undoIt();

		// here be careful of logic of all=true vs all=false (cf. hubiC/Reference/Testing/truth-testing-note_IMPORTANT.txt)
		assertTrue("all first undoes must be false!", !dataDel0OK & !dataDel1OK & !dataDel2OK);

		assertTrue("del2 do() " + msgDelCheck, dataDel2.doIt());
		assertTrue("del1 do() " + msgDelCheck, dataDel1.doIt());
		assertTrue("del0 do() " + msgDelCheck, dataDel0.doIt());
		assertFalse("del2 do() again " + msgDelCheck, dataDel2.doIt());
		assertFalse("del1 do() again " + msgDelCheck, dataDel1.doIt());
		assertFalse("del0 do() again " + msgDelCheck, dataDel0.doIt());
		assertTrue("is done 0 " + msgDelCheck, dataDel0.hasBeenDone());
		assertTrue("is done 1 " + msgDelCheck, dataDel1.hasBeenDone());
		assertTrue("is done 2 " + msgDelCheck, dataDel2.hasBeenDone());
		System.out.println("after removals: " + dm.toString());

		assertTrue("del0 undo() " + msgDelCheck, dataDel0.undoIt());
		assertTrue("del1 undo() " + msgDelCheck, dataDel1.undoIt());
		assertTrue("del2 undo() " + msgDelCheck, dataDel2.undoIt());
		assertFalse("del0 undo() again " + msgDelCheck, dataDel0.undoIt());
		assertFalse("del1 undo() again " + msgDelCheck, dataDel1.undoIt());
		assertFalse("del2 undo() again " + msgDelCheck, dataDel2.undoIt());
		assertTrue("is undone 0 " + msgDelCheck, !dataDel0.hasBeenDone());
		assertTrue("is undone 1 " + msgDelCheck, !dataDel1.hasBeenDone());
		assertTrue("is undone 2 " + msgDelCheck, !dataDel2.hasBeenDone());
		System.out.println("after undoing removals: " + dm.toString());
		
		Logger.getGlobal().info("**** Ending testEditDataModelRecords()\n");
	}

}
