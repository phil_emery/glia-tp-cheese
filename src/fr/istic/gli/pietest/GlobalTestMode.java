/**
 * 
 */
package fr.istic.gli.pietest;

import java.util.logging.Logger;

/**
 * Use in classes to facilitate specific behaviour needed during unit testing.
 * 
 * Note: a future version of this class could implement different test mode levels, say four or five
 *       (a bit like the Logger levels that don't work properly).
 * 
 * @author Jurgen
 *
 */
public class GlobalTestMode {

	private static boolean isTestMode = false;
	
	/**
	 * @param isTestMode defines whether this class is in test mode, or not
	 */
	public static void setIsTestMode(boolean isTestMode) {
		if (!GlobalTestMode.isTestMode && isTestMode) {
			Logger.getGlobal().info("*** Turning on Test Mode ***");
		}
		else if (GlobalTestMode.isTestMode && !isTestMode) {
			Logger.getGlobal().info("*** Turning off Test Mode ***");
		}
		else if (GlobalTestMode.isTestMode && isTestMode) {
			Logger.getGlobal().info("Test Mode is already turned on");
		}
		GlobalTestMode.isTestMode = isTestMode;
	}

	/**
	 * @return true if this class is in test mode, or false if not
	 */
	public static boolean getIsTestMode() {
		return GlobalTestMode.isTestMode;
	}

	/**
	 * @param message is printed to Logger.getGlobal().info(message) only if isTestMode is true
	 * @return isTestMode
	 */
	public static boolean getIsTestMode(String message) {
		if (GlobalTestMode.isTestMode) {
			if (message!=null && !message.isEmpty()) {
				Logger.getGlobal().info(message);
			}
			else {
				Logger.getGlobal().info("<no message>");
			}
		}
		return GlobalTestMode.isTestMode;
	}
}
