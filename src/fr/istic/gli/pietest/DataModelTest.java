/**
 * 
 */
package fr.istic.gli.pietest;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.istic.gli.data.DataModel;
import fr.istic.gli.data.Record;

/**
 * Data Model test : functional Use Cases
 * @author Jurgen
 */
public class DataModelTest {

	public static DataModel dm1 = null;
	public static DataModel dm2 = null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Logger.getGlobal().setLevel(Level.ALL);
		Logger.getGlobal().info("setting up Data Model test class");
		dm1 = new DataModel("dm1");
		dm2 = new DataModel("dm2");
		
		GlobalTestMode.setIsTestMode(true);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Logger.getGlobal().info("closing Data Model test class");
		
		GlobalTestMode.setIsTestMode(false);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	public void invariant() throws Exception {
		assertNotEquals(dm2, dm1);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testPopulatingDM() throws Exception {
		Logger.getGlobal().info("Starting testPopulatingDM()");
		invariant();
		System.out.println(dm1.toString());
		System.out.println(dm2.toString());
		
		Record r1 = new Record("r1", 21.22);
		Record r2 = new Record("r2", 22.22);
		Record r3 = new Record("r3", 23.22);
		Record r4 = new Record("r4", 24.22);
		Record r5 = new Record("r5", 25.22);

		System.out.println(r1.toString()
				 + "\n" + r2.toString()
				  + "\n" + r3.toString()
				   + "\n" + r4.toString()
				    + "\n" + r5.toString());
		
		long kr1 = dm1.addItem(r1);
		long kr2 = dm1.addItem(r2);
		long kr3 = dm1.addItem(r3);
		long kr3again = dm1.addItem(r3);
		assertFalse(kr3==kr3again);
		assertTrue(dm1.readItem(kr3).equals(dm1.readItem(kr3again)));

		dm1.setDataModelName(dm1.getDataModelName() + " ...renamed");
		
		long kr4 = dm1.addItem(r4);
		long kr5 = dm1.addItem(r5);
		
		r1.setLabel("r1-renamed");
		r1.setContents(21.220022);
		dm2.updateItem(kr1, r1);
		System.out.println(dm1.toString());
		assertNotNull(dm1.readItem(kr1));
		System.out.println(dm1.readItem(kr1).toString());

		assertTrue(dm1.deleteItem(kr2));
		System.out.println("r2a removed --> " + r2.toString());
		System.out.println(dm1.toString());

		assertTrue(dm2.addItem(r2)!=Record.unidentifiedKey);
		System.out.println(dm2.toString());
		
		for (long k : dm1.getKeys()) {
			Record r = dm1.readItem(k);
			assertTrue(dm1.deleteItem(k));
			System.out.println("Removed ---> " + r.toString());
		}
		System.out.println(dm1.toString());

		for (long k : dm2.getKeys()) {
			Record r = dm2.readItem(k);
			assertTrue(dm2.deleteItem(k));
			System.out.println("Removed ---> " + r.toString());
		}
		System.out.println(dm2.toString());

		r1 = new Record("testing-r1", 51.22);
		r2 = new Record("another for r2", 62.22);
		r3 = new Record("and one more at r3", 93.22);
		r4 = new Record("could be the end with r4", 44.22);
		r5 = new Record("but it wasn't, given r5", 55.22);
		Record r6 = new Record("And the new r6!", 51.22); // identical contents to "r1"
		Record r7 = new Record("and one more at r3", 97.22); // identical label to "r3"

		assertNotEquals(Record.unidentifiedKey, dm1.addItem(r1));
		assertNotEquals(Record.unidentifiedKey, dm1.addItem(r2));
		assertNotEquals(Record.unidentifiedKey, dm1.addItem(r3));
		assertNotEquals(Record.unidentifiedKey, dm1.addItem(r4));
		assertNotEquals(Record.unidentifiedKey, dm1.addItem(r5));
		assertNotEquals(Record.unidentifiedKey, dm1.addItem(r6));
		assertNotEquals(Record.unidentifiedKey, dm1.addItem(r7));
		System.out.println("\nLast dm1 without any sorting: \n" + dm1.toString() + "\n");

		LinkedList<Long> klLabels = dm1.getKeysOrderByLabels();
		System.out.println("\ndm1 keys sorted by labels: \n    " + klLabels.toString());
		for (int i=0; i<klLabels.size(); i++) {
			System.out.println("  " + dm1.getDataModelName() + "(" + i + "): \""
					+ (r1=dm1.readItem((long)klLabels.get(i))).getLabel() + "\", "
					+ r1.getContents().toString());
		}			

		LinkedList<Long> klContents = dm1.getKeysOrderByContents();
		System.out.println("\ndm1 keys sorted by contents: \n    " + klContents.toString());
		for (int i=0; i<klContents.size(); i++) {
			System.out.println("  " + dm1.getDataModelName() + "(" + i + "): \""
					+ (r1=dm1.readItem((long)klContents.get(i))).getLabel() + "\", "
					+ r1.getContents().toString());
		}			
		
		System.out.println("");
	}

}
