/**
 * 
 */
package fr.istic.gli.pietest;

import static org.junit.Assert.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.istic.gli.data.ConsoleViewer;
import fr.istic.gli.data.DataModel;
import fr.istic.gli.data.Record;

/**
 * Data Model test : functional Use Cases
 * @author Jurgen
 */
public class ViewerTest {

	public static DataModel dm = null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Logger.getGlobal().setLevel(Level.ALL);
		Logger.getGlobal().info("setting up Viewer test class");
		dm = new DataModel("Shared Data Model");
		
//		GlobalTestMode.setIsTestMode(true);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Logger.getGlobal().info("closing Viewer test class");
		
//		GlobalTestMode.setIsTestMode(false);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	public void invariant() throws Exception {
		assertNotEquals(null, dm);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testViewingDM() throws Exception {
		Logger.getGlobal().info("Starting testViewingDM()");
		invariant();
		System.out.println(dm.toString());

		ConsoleViewer cv = null;
		
		try {
			cv = new ConsoleViewer(null);
		}
		catch(Exception e) {
			Logger.getGlobal().info(e.getMessage());
			Logger.getGlobal().info("caught ConsoleViewer constructor protection successfully");
		}
		
		cv = new ConsoleViewer(dm);
		
		Record r1 = new Record("r1", 21.22);
		Record r2 = new Record("r2", 22.22);
		Record r3 = new Record("r3", 23.22);
		Record r4 = new Record("r4", 24.22);
		Record r5 = new Record("r5", 25.22);

		System.out.println(r1.toString()
				 + "\n" + r2.toString()
				  + "\n" + r3.toString()
				   + "\n" + r4.toString()
				    + "\n" + r5.toString());
		
		long kr1 = dm.addItem(r1);
		long kr2 = dm.addItem(r2);
		long kr3 = dm.addItem(r3);
		long kr3again = dm.addItem(r3);
		assertFalse(kr3==kr3again);
		assertTrue(dm.readItem(kr3).equals(dm.readItem(kr3again)));

		dm.setDataModelName(dm.getDataModelName() + " ...renamed");
		
		long kr4 = dm.addItem(r4);
		long kr5 = dm.addItem(r5);
		
		r1.setLabel("r1-renamed");
		r1.setContents(21.220022);
		dm.updateItem(kr1, r1);
		assertNotNull(dm.readItem(kr1));
		System.out.println(dm.toString());
		System.out.println(dm.readItem(kr1).toString());

		assertTrue(dm.deleteItem(kr2));
		System.out.println("r2a removed --> " + r2.toString());
		System.out.println(dm.toString());
		
		for (long k : dm.getKeys()) {
			Record r = dm.readItem(k);
			assertTrue(dm.deleteItem(k));
			System.out.println(r.toString());
		}
		System.out.println(">>>after deleting all records: \n " + dm.toString());

		DataModel dm2 = new DataModel("Second shared Data Model");
		kr1 = dm2.addItem(r1);
		kr2 = dm2.addItem(r2);
		kr3 = dm2.addItem(r3);
		kr4 = dm2.addItem(r4);
		kr5 = dm2.addItem(r5);
		cv.setViewedDM(dm2);
		
		r5.setLabel("updated r5 label");
		assertTrue(dm2.updateItem(kr5, r5));

		
	}

}
