package fr.istic.gli.data;

import java.security.InvalidParameterException;

import fr.istic.gli.interfaces.AbstractEditHostedItem;

/**
 * 
 * @author Jurgen
 *
 */
public class EditDataModelRemove extends AbstractEditHostedItem<DataModel, Long>{

	private long keyToRemove;
	private Record itemValue;
	
	/**
	 * @param host data model
	 * @param key of Record to be removed (must be in host when this command is created)
	 * @throws InvalidParameterException if key Record is not in <code>host</code>
	 */
	public EditDataModelRemove(DataModel host, Long key) throws InvalidParameterException {
		super(host, key);
		this.keyToRemove = key;
		if (this.keyToRemove==Record.unidentifiedKey || this.refreshItemValue()==null) {
			throw new InvalidParameterException("EditRecordRemove: record not in host");
		}
	}

	/**
	 * Utility function to update latest Record 
	 * @return the current state of this.itemValue
	 */
	private Record refreshItemValue() {
		// protect against losing values if refreshing whilst item is deleted from host
		Record tempR = this.getHost().readItem(this.keyToRemove);
		return ((tempR!=null) ? (this.itemValue=tempR) : this.itemValue);
	}
	
	/**
	 * This function returns <code>Record.unidentifiedKey</code> if <code>host</code> is null. <br/>
	 * Otherwise it returns the current value of the <code>keyToRemove</code> (which may not be the original key declared to the
	 * command, if any number of undos have been applied). 
	 */
	@Override
	public Long getHostedItem(DataModel host) {
		if (host!=null) {
			return makeNewItem(this.keyToRemove);
		}
		return Record.unidentifiedKey;
	}

	/**
	 * This factory operation generates a new Long.
	 */
	@Override
	public Long makeNewItem(Long item) {
		return (item!=null ? new Long(this.keyToRemove) : Record.unidentifiedKey);
	}

	/**
	 * This procedure remains silent if the DataModel argument is null. The second argument is ignored.
	 */
	@Override
	public void setHostedItemValue(DataModel host, Long item) {
		if (host!=null) {
			if (!this.hasBeenDone()) {
				this.refreshItemValue();
				host.deleteItem(this.keyToRemove); // ignore response
				this.keyToRemove = Record.unidentifiedKey; // discard previous value
			}
			else {
				this.keyToRemove = host.addItem(this.refreshItemValue());
			}
		}
		// else do nothing
	}

}
