package fr.istic.gli.data;

import java.security.InvalidParameterException;

import fr.istic.gli.interfaces.AbstractEditHostedItem;

/**
 * 
 * @author Jurgen
 *
 */
public class EditRecordContents extends AbstractEditHostedItem<Record, Double>{

	public EditRecordContents(Record host, Double item) throws InvalidParameterException {
		super(host, item);
	}

	/**
	 * This function returns 0.0 if <code>host</code> is null.
	 */
	@Override
	public Double getHostedItem(Record host) {
		if (host!=null) {
			return this.makeNewItem(host.getContents());
		}
		else {
			return 0.0;
		}
	}

	/**
	 * This factory operation generates a new Double object.
	 */
	@Override
	public Double makeNewItem(Double item) {
		return new Double((item==null) ? 0.0 : item);
	}

	/**
	 * This procedure remains silent if either of its arguments is null.
	 */
	@Override
	public void setHostedItemValue(Record host, Double item) {
		if (host!=null && item!=null) {
			host.setContents(item);
		}
		// else do nothing
	}

}
