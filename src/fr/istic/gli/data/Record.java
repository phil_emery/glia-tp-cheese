package fr.istic.gli.data;

import fr.istic.gli.interfaces.IRecord;

/**
 * Implementation of record for data model.
 * NOTE1: When Records are part of a DataModel, then their change events for viewable data (label or contents)
 *        are notified to their host DataModel via the simple procedure DataModel.recordInform(this).
 * NOTE2: This object is a "Receiver" for the Record edit command objects.
 *  
 * @author Jurgen
 *
 */
public class Record implements IRecord {
	public final static long unidentifiedKey = 0;
	final static String noLabel = "<no label>";
	
	private long key = unidentifiedKey;
	private String label = noLabel;
	private Double contents = 0.0;
	private DataModel hostDM= null;
	
	/**
	 * key and label parameters for this Record are set to default values 'noKey' and 'noLabel'
	 * @param contents
	 */
	public Record(Double contents) {
		this.setHostDataModel(null);
		this.setKey();
		this.setLabel(noLabel);
		this.setContents(contents);
	}
	
	/**
	 * key parameter for this Record is set to default value 'noKey'
	 * @param label
	 * @param contents
	 */
	public Record(String label, Double contents) {
		this.setHostDataModel(null);
		this.setKey();
		this.setLabel(label);
		this.setContents(contents);
	}

	@Override
	public String toString() {
		return "Record: label=\"" + this.label + "\", contents=" + this.contents + ", key=" + this.key
				+ ", " + (hostDM==null ? "isolated" : "has host called \"" + hostDM.getDataModelName() + "\"");
	}
	
	/**
	 * @return the hostDM
	 */
	public DataModel getHostDataModel() {
		return hostDM;
	}

	/**
	 * @param hostDM the hostDM to set
	 */
	public void setHostDataModel(DataModel hostDM) {
		this.hostDM = hostDM;
	}

	@Override
	public boolean setKey() {
		if (this.hostDM==null) {
			this.key = Record.unidentifiedKey;
			return false;
		}
		else {
			this.key = this.getHostDataModel().getNextKey();
			return true;
		}
	}

	@Override
	public long getKey() {
		return this.key;
	}

	/**
	 * The argument is cloned before being attached as <code>label</code>.
	 */
	@Override
	public void setLabel(String newLabel) {
		boolean changed = !this.label.equals(newLabel);
		
		if (changed) {
			this.label = (newLabel==null) ? "" : new String(newLabel);
		}

		// notify AFTER set transaction is complete
		if (changed && this.hostDM!=null) { // isolated Records, without a host DataModel, are not visible to Viewers
			this.hostDM.recordInform(this);
		}		
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	@Override
	public void setContents(Double newContents) {
		boolean changed = !this.contents.equals(newContents);

		if (changed) {
			this.contents = newContents;
		}
		
		// notify AFTER set transaction is complete
		if (changed && this.hostDM!=null) { // isolated Records, without a host DataModel, are not visible to Viewers
			this.hostDM.recordInform(this);
		}		
	}

	@Override
	public Double getContents() {
		return this.contents;
	}

	/**
	 * Compare data values of <code>this</code> Record with that of another Record
	 * @param otherRecord
	 * @return true if data contents are the same between <code>this</code> and <code>otherRecord</code>, false otherwise
	 */
	public boolean equals(Record otherRecord) {
		return (this.getLabel().compareTo(otherRecord.getLabel())==0 &&
				this.getContents()==otherRecord.getContents());
	}
}
