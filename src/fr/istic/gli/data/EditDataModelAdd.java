package fr.istic.gli.data;

import java.security.InvalidParameterException;

import fr.istic.gli.interfaces.AbstractEditHostedItem;

/**
 * 
 * @author Jurgen
 *
 */
public class EditDataModelAdd extends AbstractEditHostedItem<DataModel, Record>{

	private long keyForRemoval;
	private final Record itemValue;
	
	/**
	 * @param host data model
	 * @param item is Record with values to be added (note that duplicate Record values are permitted)
	 * @throws InvalidParameterException if either <code>Record</code> or <code>host</code> is null
	 */
	public EditDataModelAdd(DataModel host, Record item) throws InvalidParameterException {
		super(host, item);
		if (item==null) {
			throw new InvalidParameterException("EditRecordAdd: record is null");
		}
		else {
			this.keyForRemoval = Record.unidentifiedKey;
			this.itemValue = this.makeNewItem(item);
		}
	}
	
	/**
	 * This function returns a copy of the item for addition only if the addition has been applied. Otherwise it returns null. 
	 */
	@Override
	public Record getHostedItem(DataModel host) {
		if (host!=null && this.hasBeenDone()) {
			return makeNewItem(this.itemValue);
		}
		return null;
	}

	/**
	 * This factory operation generates a new copy of <code>item</code>.
	 */
	@Override
	public Record makeNewItem(Record item) {
		return (item!=null ? new Record(item.getLabel(), item.getContents()) : null);
	}

	/**
	 * This procedure remains silent if the DataModel argument is null. The second argument is ignored.
	 */
	@Override
	public void setHostedItemValue(DataModel host, Record item) {
		if (host!=null) {
			if (!this.hasBeenDone()) {
				this.keyForRemoval = host.addItem(this.itemValue);
			}
			else {
				host.deleteItem(this.keyForRemoval); // ignore response
				this.keyForRemoval = Record.unidentifiedKey; // discard previous value
			}
		}
		// else do nothing
	}

}
