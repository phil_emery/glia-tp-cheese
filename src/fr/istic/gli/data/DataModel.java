/**
 * 
 */
package fr.istic.gli.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;

import fr.istic.gli.interfaces.IDataModel;
import fr.istic.gli.pietest.GlobalTestMode;

/**
 * Implementation of data model
 * 
 * NOTE1: This Data Model is Observable, and its modifications are observed by Viewers. It is composed individual
 *        constituent Records that may be modified independently of this their host DataModel. The constituent
 *        Records are not directly observable, but instead they use recordInform() to let their DataModel know
 *        of their updates. Their DataModel then manages how its Viewers are notified.
 *        
 * NOTE2: Defensive copies of mutable Record items are made, whenever necessary (saw this on www.javapractices.com's
 *        "Implementing Serializable"), so that only item data can go in and out of this Data Model. The item objects
 *        are thereby protected from unwitting change by external code, and the integrity of this Data Model is guaranteed.
 * 
 * @author Jurgen
 *
 */
public class DataModel extends Observable implements IDataModel, Serializable {
	private static final long serialVersionUID = 8668282383263968252L;
	private static final String defaultDataModelName = "Data Model";
	/**
	 * @serial
	 */
	private String dataModelName = ""; // observed
	private static long firstKey = 1;
	/**
	 * @serial
	 */
	private long nextKey = firstKey;

	/**
	 * @serial
	 */
	private Map<Long, Record> records = null; // observed

	private boolean recordInformed = false;
	
	/**
	 * creates new empty DataModel object
	 * @param modelName is assigned to this data model
	 */
	public DataModel(String modelName) {
		this.setDataModelName(modelName);
		this.records = new HashMap<Long, Record>();
	}
	
	/**
	 * Default output order for Data Model contents is sequential by increasing Record key value.
	 */
	@Override
	public String toString() {
		String retStr = ", with records:";
		if (this.records.isEmpty()) {
			retStr = retStr + " <no records found>";
		}
		else {
			for (Entry<Long, Record> e : this.records.entrySet()) {
				retStr = retStr + "\n    [" + e.getValue().toString() + "]";
			}
		}
		return "DataModel: name=\"" + this.dataModelName + "\"" + retStr;
	}
	
	/**
	 * @return the dataModelName
	 */
	public String getDataModelName() {
		return dataModelName;
	}

	/**
	 * @param dataModelName the dataModelName to set
	 */
	public void setDataModelName(String dataModelName) {
		boolean changed = !this.dataModelName.equals(dataModelName);
		
		if (changed) {
			this.dataModelName = new String(dataModelName); // defensive new copy
		}
		else if (dataModelName.isEmpty()) {
			this.dataModelName = defaultDataModelName;
			changed = true;
		}

		// notify AFTER set transaction is complete
		if (changed) {
			this.setChanged();
			GlobalTestMode.getIsTestMode("new Data Model name is \"" + dataModelName + "\"");
			this.notifyObservers(dataModelName);
		}
	}

	/**
	 * Simple function for obtaining a new unique key identifier
	 * @return next available key identifier
	 */
	public long getNextKey() {
		long retKey = this.nextKey;
		this.nextKey++;
		return retKey;
	}

	/**
	 * @return keys of this Data Model as iterable Collection of 'long' values
	 */
	public Collection<Long> getKeys() {
		// Here we need to return a cloned copy of the key set. This is because the iterable keySet() is a separate object to
		// the Map<K, V>. The Set and the Map are maintained in synchronous with each other by the Java Utils library. Any
		// write operation on the 'keySet()', such as deleteItem(), will result in 'java.util.ConcurrentModificationException'.
		Collection<Long> retCol = new ArrayList<Long>();
		for (Long k : this.records.keySet()) {
			Long l = new Long(k);
			retCol.add(l);
		}
		return retCol;
	}
	
	@Override
	public long addItem(Record r) {
		if (r!=null && r.getHostDataModel()==null) {
			Record rCopy = new Record(r.getLabel(), r.getContents());
			rCopy.setHostDataModel(this);
			if (rCopy.setKey()) {
				this.records.put(rCopy.getKey(), rCopy);

				// notify AFTER add transaction is complete, and only if Record has not yet notified
				if (!this.hasChanged()) {
					if (!this.recordInformed) {
						this.setChanged();
						GlobalTestMode.getIsTestMode("Record added: \n" + rCopy.toString());
						this.notifyObservers();
					}
					else {
						this.recordInformed = false;
					}
				}
				
				return rCopy.getKey();
			}
			else {
				rCopy = null;
				return Record.unidentifiedKey;
			}
		}
		else {
			return Record.unidentifiedKey;
		}
	}

	@Override
	public Record readItem(long key) {
		Record r = this.records.get((Long)key);
		return r!=null ? new Record(r.getLabel(), r.getContents()) : null; // defensive new object
	}

	@Override
	public boolean updateItem(long key, Record r) {
		Record actualR = this.records.get((Long)key);
		if (actualR!= null && r!=null) {
			if (actualR.getLabel().compareTo(r.getLabel())!=0) {
				actualR.setLabel(r.getLabel());
			}
			actualR.setContents(r.getContents());

			// notify AFTER update transaction is complete, and only if Record has not yet notified
			if (!this.hasChanged()) {
				if (!this.recordInformed) {
					this.setChanged();
					GlobalTestMode.getIsTestMode("Record updated: \n" + actualR.toString());
					this.notifyObservers((Long)key);
				}
				else {
					this.recordInformed = false;
				}
			}

			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean deleteItem(long key) {
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // !!! TODO !!! Need to invent mechanism of key reservation key for Records that may need to be "restored" (eg. through undo). //
  // eg. boolean deleteItemResKey(long key) with normal deleteItem() cancelling the reservation
 //		 boolean addItem(Record r, long resKey) false if no reservation, else addition is performed
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		Record tempR = this.records.remove((Long)key);
		if (tempR != null) {
			tempR.setHostDataModel(null);
			boolean retVal = !tempR.setKey(); // cancels key since host DataModel is now 'null'

			// notify AFTER delete transaction is complete, and only if Record has not yet notified
			if (!this.hasChanged()) {
				this.setChanged();
				GlobalTestMode.getIsTestMode("Record removed: \n" + tempR.toString());
				this.notifyObservers();
			}
			
			return retVal;
		}
		else {
			return false;
		}
	}

	/**
	 * This procedure allows a constituent Record to notify its host DataModel that it has a change to notify to any
	 * interested observers. The procedure does nothing, silently, if Record is not in this DataModel.
	 * @param r be a constituent Record of this DataModel
	 */
	public void recordInform(Record r) {
		if (r!=null && r.getHostDataModel()!=null && r.getHostDataModel()==this) {
			this.recordInformed = true;
			this.setChanged();
			GlobalTestMode.getIsTestMode("Record-specific notification: \n" + r.toString());
			this.notifyObservers((Long)r.getKey());
		}
	}

	
	/************************************************
	 * utility operations for easier view processing
	 * 
	 */

	private LinkedList<Long> keyListByLabels;
	private LinkedList<Long> keyListByContents;
	
	/**
	 * Builds list of keys ordered by increasing Record label lexicographical values.
	 * @param dm
	 * @return list of keys in ascending order of record label strings
	 */
	public LinkedList<Long> getKeysOrderByLabels() {
		if (keyListByLabels==null) {
			keyListByLabels = new LinkedList<Long>();
		}
		keyListByLabels.clear();
		
		for (long k : this.getKeys()) {
			if (keyListByLabels.isEmpty()) {
				keyListByLabels.add((Long)k);
			}
			else {
				int i;
				for (i=0; i<keyListByLabels.size() &&
						this.readItem(k).getLabel().compareTo(this.readItem(keyListByLabels.get(i)).getLabel()) > 0;
						i++) {
				}
				// Now at the "i"th position, just after the last smaller target. NB: "i" == size() is legal.
				keyListByLabels.add(i, (Long)k);
			}
		}
		
		return keyListByLabels;
	}


	/**
	 * Builds list of keys ordered by increasing Record content values.
	 * @param dm
	 * @return list of keys in ascending order of record content values
	 */
	public LinkedList<Long> getKeysOrderByContents() {
		if (keyListByContents==null) {
			keyListByContents = new LinkedList<Long>();
		}
		keyListByContents.clear();
		
		for (long k : this.getKeys()) {
			if (keyListByContents.isEmpty()) {
				keyListByContents.add((Long)k);
			}
			else {
				int i;
				for (i=0; i<keyListByContents.size() &&
						this.readItem(k).getContents() > this.readItem(keyListByContents.get(i)).getContents();
						i++) {
				}
				// Now at the "i"th position, just after the last smaller target. NB: "i" == size() is legal.
				keyListByContents.add(i, (Long)k);
			}
		}
		
		return keyListByContents;
	}

}
