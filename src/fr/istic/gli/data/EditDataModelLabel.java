package fr.istic.gli.data;

import java.security.InvalidParameterException;

import fr.istic.gli.interfaces.AbstractEditHostedItem;

/**
 * 
 * @author Jurgen
 *
 */
public class EditDataModelLabel extends AbstractEditHostedItem<DataModel, String>{

	public EditDataModelLabel(DataModel host, String item) throws InvalidParameterException {
		super(host, item);
	}

	/**
	 * This function returns an empty string if <code>host</code> is null.
	 */
	@Override
	public String getHostedItem(DataModel host) {
		if (host!=null) {
			return this.makeNewItem(host.getDataModelName());
		}
		else {
			return "";
		}
	}

	/**
	 * This factory operation generates a new string.
	 */
	@Override
	public String makeNewItem(String item) {
		return new String((item==null) ? "" : item);
	}

	/**
	 * This procedure remains silent if either of its arguments is null.
	 */
	@Override
	public void setHostedItemValue(DataModel host, String item) {
		if (host!=null && item!=null) {
			host.setDataModelName(item);
		}
		// else do nothing
	}

}
