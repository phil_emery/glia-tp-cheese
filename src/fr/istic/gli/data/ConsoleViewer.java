package fr.istic.gli.data;

import java.util.Observable;
import fr.istic.gli.pietest.GlobalTestMode;

/**
 * Implementation of Console-specific Viewer
 * NOTE: This very basic viewer allows updating Data Model to console output every time there is a notification
 *       of the observed Data Model. Subscription to these notifications is handled by a separate Controller. 
 * 
 * @author Jurgen
 *
 */
public class ConsoleViewer extends Viewer {

	public ConsoleViewer(DataModel dm) throws Exception {
		super(dm);
		// TODO console-specific construction !!!!!!!!!!!!!!!!!!
	}
	
	@Override
	public void update(Observable dm, Object arg) {
		if (GlobalTestMode.getIsTestMode(">>>ConsoleViewer<<<")) {
			super.update(dm, arg);			
		}
		
		if (dm!=null && dm==this.getViewedDM()) {
			if (arg instanceof Long) {
				showRecord((DataModel)dm, (long)arg);
			}
			else if (arg instanceof String) {
				showDataModelName((String)arg);				
			}
			else {
				showDataModel((DataModel)dm);
			}
		}
	}

	private void showRecord(DataModel dm, long key) {
		Record record = null;
		if (dm!=null && (record=dm.readItem(key))!=null) {
			System.out.println("\nconsole Viewer shows Record update: \n" + record.toString());
		}
		else {
			GlobalTestMode.getIsTestMode("showRecord: !!! record not in data model !!!");
		}
	}

	private void showDataModelName(String dmName) {
		if (dmName!=null) {
			System.out.println("\nconsole Viewer shows DataModel name update: \"" + dmName + "\"");
		}
		else {
			GlobalTestMode.getIsTestMode("showRecord: !!! DataModel name is null !!!");
		}
	}


	private void showDataModel(DataModel dm) {
		if (dm!=null) {
			System.out.println("\nconsole Viewer shows DataModel update: \n" + dm.toString());
		}
		else {
			GlobalTestMode.getIsTestMode("showRecord: !!! DataModel is null !!!");
		}
	}
	
}
