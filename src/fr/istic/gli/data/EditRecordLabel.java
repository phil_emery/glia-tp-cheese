package fr.istic.gli.data;

import java.security.InvalidParameterException;

import fr.istic.gli.interfaces.AbstractEditHostedItem;

/**
 * 
 * @author Jurgen
 *
 */
public class EditRecordLabel extends AbstractEditHostedItem<Record, String>{

	public EditRecordLabel(Record host, String item) throws InvalidParameterException {
		super(host, item);
	}

	/**
	 * This function returns an empty string if <code>host</code> is null.
	 */
	@Override
	public String getHostedItem(Record host) {
		if (host!=null) {
			return this.makeNewItem(host.getLabel());
		}
		else {
			return "";
		}
	}

	/**
	 * This factory operation generates a new string.
	 */
	@Override
	public String makeNewItem(String item) {
		return new String((item==null) ? "" : item);
	}

	/**
	 * This procedure remains silent if either of its arguments is null.
	 */
	@Override
	public void setHostedItemValue(Record host, String item) {
		if (host!=null && item!=null) {
			host.setLabel(item);
		}
		// else do nothing
	}

}
