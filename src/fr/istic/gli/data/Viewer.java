package fr.istic.gli.data;

import java.security.InvalidParameterException;
import java.util.Observable;
import java.util.logging.Logger;

import fr.istic.gli.interfaces.IViewer;
import fr.istic.gli.pietest.GlobalTestMode;

/**
 * Implementation of generic viewer
 * NOTE1: This is the generic Viewer that subscribes to and receives notifications of an observed Data Model. The viewed
 *        DataModel is mandatory and a missing DataModel throws an Exception.
 * NOTE2: A future version of this class could cater for Viewers that are capable of processing updates at the Record
 *        level. Today this is not the case and since current Viewers refresh the entire DataModel for every change.
 *        An idea for this implementation is to make Record Observable and have DataModel manage the subscription of its
 *        Viewers to its constituent Records.
 * 
 * @author Jurgen
 *
 */
public class Viewer implements IViewer {
	
	private DataModel viewedDM = null; // must never be null in instance of this Viewer
	
	/**
	 * key and label parameters for this Record are set to default values 'noKey' and 'noLabel'
	 * @param contents
	 * @throws Exception 
	 */
	public Viewer(DataModel dm) throws Exception {
		try {
			this.setViewedDM(dm);
		}
		catch(Exception e) {
			Logger.getGlobal().warning("Generic Viewer constructor error: viewed DataModel has been rejected");
			throw e;
		}
	}

	@Override
	public DataModel getViewedDM() {
		return viewedDM;
	}

	@Override
	public void setViewedDM(DataModel dm) throws Exception {
		if (dm == null) {
			throw new InvalidParameterException("Generic Viewer setViewedDM error: viewed DataModel reference is null");
		}
		else {
			if (this.viewedDM != null) {
				this.viewedDM.deleteObserver(this); // that is, the viewed DataModel is changing
			}
			this.viewedDM = dm;
			this.viewedDM.addObserver(this);
			if (GlobalTestMode.getIsTestMode()) {
				this.update(dm, "newly attached DataModel:\n" + dm.toString());
			}
			else {
				this.update(dm, null);
			}
		}
	}

	@Override
	public void update(Observable dm, Object arg) {
		if (dm!=null && dm==this.getViewedDM()) {
			Logger.getGlobal().info("generic Viewer update called with arg: " + (arg==null ? "<null>" : "\n" + arg.toString()));
		}
		// specific implementation depends on Viewer-derived classes
	}
	

}
