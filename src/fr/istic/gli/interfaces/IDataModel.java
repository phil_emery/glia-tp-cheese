/**
 * 
 */
package fr.istic.gli.interfaces;

import fr.istic.gli.data.Record;

/**
 * Simple "CRUD" data model using Record items with unique identifier keys.
 *
 */
public interface IDataModel {
	/**
	 * Adds data to this DataModel.
	 * @param r must not already be in a DataModel object
	 * @return either new key attribute, <code>Record.unidentifiedKey</code> if r is already in a DataModel object
	 *         or some other problem occurred
	 */
	public long addItem(Record r);

	/**
	 * 
	 * @param key must be a valid Key in this DataModel.
	 * @return Record, if key is found, null otherwise.
	 */
	public Record readItem(long key);
	
	/**
	 * 
	 * @param key must be the key of an existing Record in this DataModel and which remains after this operation
	 * @param r will have its data values replace those of the Record identified by <code>key</code> in this DataModel.
	 * @return true if update was performed, false otherwise.
	 */
	public boolean updateItem(long key, Record r);
	
	/**
	 * Remove item from storage
	 * @param key is unique
	 * @return true if item is successfully removed, false if item not found or there was another problem
	 */
	public boolean deleteItem(long key);

}
