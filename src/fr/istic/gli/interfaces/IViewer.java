package fr.istic.gli.interfaces;

import java.util.Observer;

import fr.istic.gli.data.DataModel;

public interface IViewer extends Observer {

	/**
	 * @return the DataModel being observed by this Viewer
	 */
	public DataModel getViewedDM();
	
	/**
	 * This procedure adds this Viewer to the list of Observers that DataModel dm notifies when it has changes
	 * in viewable data.
	 * @param dm is the Observable DataModel
	 * @throws Exception is thrown if dm is null, because that is a real "show stopper"
	 */
	public void setViewedDM(DataModel dm) throws Exception;
	
}
