package fr.istic.gli.interfaces;

public interface IRecord {
	/**
	 * setKey assigns the next available key from the DataModel hosting this record, or does nothing if this record is isolated
	 * @return true if key set, false if key not set
	 */
	public boolean setKey();
	
	/**
	 * 
	 * @return key of this record. If key is zero, then this record is not currently in a DataModel object.
	 */
	public long getKey();
	
	/**
	 * @param newLabel becomes label of this Record
	 */
	public void setLabel(String newLabel);
	
	/**
	 * @return the label of this Record
	 */
	public String getLabel();
	
	/**
	 * @param newContents becomes the numerical value of this Record 
	 */
	public void setContents(Double newContents);
	
	/**
	 * @return numerical value of this Record
	 */
	public Double getContents();
}
