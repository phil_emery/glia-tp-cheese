package fr.istic.gli.interfaces;

/**
 * Command interface for edit commands.
 * 
 */
public interface ICommand {
	
	/**
	 * @return true if this command can be done, false if it has already been done
	 */
	public boolean doIt();
	
	/**
	 * @return true if this command can be undone, false if it has already been undone or has not yet been done.
	 */
	public boolean undoIt();
	
	/**
	 * 
	 * @return true if command has been done, false if command has not yet been done or has been undone
	 */
	public boolean hasBeenDone();
}
