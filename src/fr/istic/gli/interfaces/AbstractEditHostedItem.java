package fr.istic.gli.interfaces;

import java.security.InvalidParameterException;

/**
 * This abstract class describes a command object that intercedes to update an item in a data object. <br/>
 * After setting a new value, the command is able to return the item to the value it had immediately prior to the
 * setting operation. The setting operation can then be redone, without necessity to provide the new value again.
 * This back-and-forth value toggling can be performed any number of times. <br/>
 * The command is "disposable" and in its present form cannot be updated once created. <br/>
 * Deriving classes need to implement certain methods described here, with functionality specific to the type parametres.
 *  
 * @param <H> is the hosting data object type
 * @param <I> is the item type being hosted within the data object of type <code>H</code>
 * 
 */
public abstract class AbstractEditHostedItem<H, I> implements ICommand {

	private I newValue;
	private I previousValue;
	private H host;

	private boolean hasBeenDone;

	/**
	 * Private host-setting operation
	 * @param host is hosting data object and cannot be null
	 * @throws InvalidParameterException if <code>host</code> is null
	 */
	private void setHost(H host) throws InvalidParameterException {
		if (host == null) {
			throw new InvalidParameterException("EditHostedItem: the host data object reference cannot be null");
		}
		this.host = host;
	}
	
	/**
	 * @return the host
	 */
	public H getHost() {
		return host;
	}
	
	/**
	 * 
	 * @param host data object
	 * @param item to update in <code>host</code>
	 * @throws InvalidParameterException if <code>host</code> is null
	 */
	public AbstractEditHostedItem(H host, I item) throws InvalidParameterException {
		this.setHost(host); // this call throws the exception if "host" is null

		// defensively create independent copies here
		this.previousValue = this.makeNewItem(this.getHostedItem(host));
		this.newValue = this.makeNewItem(item);

		this.hasBeenDone = false;
	}

	/**
	 * Can be called any number of times, will only perform once
	 */
	@Override
	public boolean doIt() {
		if (!this.hasBeenDone) {
			this.setHostedItemValue(this.host, this.newValue);
			this.hasBeenDone = true;
			return true;
		}
		
		return false;
	}

	@Override
	public boolean undoIt() {
		if (this.hasBeenDone) {
			this.setHostedItemValue(this.host, this.previousValue);
			this.hasBeenDone = false;
			return true;
		}
		
		return false;
	}

	@Override
	public boolean hasBeenDone() {
		return this.hasBeenDone;
	}
	
	/**
	 * This function must be written specifically for deriving classes.
	 *  
	 * @param host holds the actual data item that is to be edited, being the <code>host</code> passed to this class's constructor
	 * @return item that holds the same data values as the hosted item that is being edited. That is, it can be either the actual
	 * data item that is hosted (with all the risk of data corruption that that implies) or a cloned copy of the actual item holding
	 * the same data values.
	 */
	abstract public I getHostedItem(H host);

	/**
	 * This factory operation must be written specifically for deriving classes.
	 *
	 * @param item is the source of data for this operation
	 * @return a cloned copy of the source item with respect to the data pertinent to this implementation (i.e. not necessarilly
	 * a complete byte for byte copy of the source item object)
	 */
	abstract public I makeNewItem(I item);

	/**
	 * This procedure must be written specifically for deriving classes.
	 *
	 * @param host is the <code>host</code> passed to this class's constructor
	 * @param item is a copy that holds the value specific to the derived implementation context, and needed for update
	 * in the hosted item
	 */
	abstract public void setHostedItemValue(H host, I item);

}
