package fr.istic.gli.samples;
	
import java.util.logging.Logger;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Scene;
import javafx.scene.effect.Reflection;
import javafx.scene.effect.Shadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;


/**
 * 
 * Testing animation: numbers moving
 * 
 */
public class SampleGeneral extends Application {

	Integer count = new Integer(0);
	static boolean hasBeenPressed = false;
	static KeyCode pressedCode = KeyCode.CLEAR;


    @Override
	public void start(Stage primaryStage) {
		
		try {
			primaryStage.setTitle("A little number game");
			
			BorderPane root = new BorderPane();
			int paneDim = 100 * 4;
			Scene scene = new Scene(root, paneDim, paneDim, Color.DARKORANGE);

			final Text t1 = new Text(25, 45, "Care to play?");
			root.getChildren().add(t1);
			t1.setFont(Font.font(java.awt.Font.SANS_SERIF, 30));
			t1.setFill(Color.DARKOLIVEGREEN);
			final Reflection ref = new Reflection();
			ref.setFraction(1.0);
			t1.setEffect(ref);
			
			final Shadow shad = new Shadow();
			shad.setColor(Color.LIGHTYELLOW);
			final Text t1shad = new Text(25, 45, "Care to play?");
			t1shad.setFont(Font.font(java.awt.Font.SANS_SERIF, 30));
			root.getChildren().add(t1shad);
			t1shad.setEffect(shad);
			
			final Rectangle r1 = new Rectangle(40, 100, 300, 300);
			r1.setArcHeight(20);
			r1.setArcWidth(20);
			r1.setFill(Color.RED);
			root.getChildren().add(r1);
			
			final Text label1 = new Text(40+40, 200+40, "4");
			label1.setFont(Font.font(java.awt.Font.SANS_SERIF, 30));
			root.getChildren().add(label1);
			
			final Text label2 = new Text(40+40, 250+40, "8");
			label2.setFont(Font.font(java.awt.Font.SANS_SERIF, 30));
			root.getChildren().add(label2);
			

			// CSS currently not used
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			
			primaryStage.show();

			final Text infoNewGame = new Text(250, 80, "infoText");
			infoNewGame.setFont(Font.font(java.awt.Font.SANS_SERIF, 14));
			root.getChildren().add(infoNewGame);
			
			
			final KeyValue kv1 = new KeyValue(label1.xProperty(), 290);
			EventHandler<ActionEvent> onEnd1 = new EventHandler<ActionEvent>() {
				@Override public void handle(ActionEvent t) {
					label1.setText("8");
				}
			};
			final KeyFrame kf1 = new KeyFrame(Duration.millis(1000), onEnd1, kv1);
			
			final KeyValue kv2 = new KeyValue(label2.xProperty(), 280);
			EventHandler<ActionEvent> onEnd2 = new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent t) {
					label2.setText("16");
				}
			};
			final KeyFrame kf2 = new KeyFrame(Duration.millis(1000), onEnd2, kv2);

			
			final Timeline movetime = new Timeline();
			movetime.getKeyFrames().add(kf1);
			movetime.getKeyFrames().add(kf2);
			Logger.getGlobal().info("before first movetime.play()");
			movetime.play(); // this is action for when key is pressed
			Logger.getGlobal().info("before second movetime.play()");
			movetime.play();
			Logger.getGlobal().info("after second movetime.play()");


			EventHandler<KeyEvent> keyPressed = new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent k) {
					//Logger.getGlobal().info("key processing is " + k.getCode() + " pending " + hasBeenPressed);
					if (!hasBeenPressed && k.getCode() == KeyCode.UP) {
						hasBeenPressed = true;
						pressedCode = KeyCode.UP;
						k.consume();
						Logger.getGlobal().info("keyCode = UP " + hasBeenPressed);
					}
					else if (!hasBeenPressed && k.getCode() == KeyCode.DOWN) {
						hasBeenPressed = true;
						pressedCode = KeyCode.DOWN;
						k.consume();
						Logger.getGlobal().info("keyCode = DOWN " + hasBeenPressed);
					}
					else if (!hasBeenPressed && k.getCode() == KeyCode.LEFT) {
						hasBeenPressed = true;
						pressedCode = KeyCode.LEFT;
						k.consume();
						Logger.getGlobal().info("keyCode = LEFT " + hasBeenPressed);
					}
					else if (!hasBeenPressed && k.getCode() == KeyCode.RIGHT) {
						hasBeenPressed = true;
						pressedCode = KeyCode.RIGHT;
						k.consume();
						Logger.getGlobal().info("keyCode = RIGHT " + hasBeenPressed);
					}
					else {
						//Logger.getGlobal().info("non-cursor keyCode is " + k.getCode() + " " + hasBeenPressed);
						k.consume();
					}
				}				
			};
			primaryStage.getScene().setOnKeyPressed(keyPressed);
			
			EventHandler<KeyEvent> keyReleased = new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent k) {
					if (hasBeenPressed && k.getCode()==pressedCode) {
						Logger.getGlobal().info("cursor key " + pressedCode + " is being released");
						hasBeenPressed = false;
						pressedCode = KeyCode.CLEAR;
						k.consume();
					}
				}				
			};
			primaryStage.getScene().setOnKeyReleased(keyReleased);


			EventHandler<MouseEvent> mouseResetGame = new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent m) {
					if (m.getButton()==MouseButton.PRIMARY) {
						Logger.getGlobal().info("mouse has been clicked over the node");
						infoNewGame.setText("hi hi hi hi");
						m.consume();

						final Stage dialog = new Stage();
						dialog.initModality(Modality.WINDOW_MODAL);
						dialog.initOwner(primaryStage);
						dialog.setTitle("Confirmation");
						BorderPane dialogBox = new BorderPane();

						final Text t1 = new Text(10, 20, "Do you really want to reset this game?");
						dialogBox.getChildren().add(t1);
						t1.setFont(Font.font(java.awt.Font.SANS_SERIF, 20));
						t1.setFill(Color.RED);

						Scene dialogScene = new Scene(dialogBox, 360, 80, Color.AQUAMARINE);
						dialog.setScene(dialogScene);
						dialog.setResizable(false);
						dialog.initStyle(StageStyle.UTILITY);
						
						// create my own rectangles with text. This is because ANY attempt to create "new Button()"
						// makes it impossible to define the primaryStage's scene.setFill(Color.DARKORANGE);

						int buttonOKposX = 90;
						int buttonOKposY = 50;
						final Text textOK = new Text(buttonOKposX-45, buttonOKposY+6, "New game");
						textOK.setFont(Font.font(java.awt.Font.SANS_SERIF, 20));
						textOK.setFill(Color.SADDLEBROWN);						
						final Ellipse buttonOK = new Ellipse(buttonOKposX, buttonOKposY, 80, 15);
						buttonOK.setFill(Color.GREENYELLOW);
						buttonOK.setStroke(Color.BURLYWOOD);
						buttonOK.setStrokeWidth(2.0);
						dialogBox.getChildren().add(buttonOK);
						dialogBox.getChildren().add(textOK);
						
						int buttonCANCELposX = 270;
						int buttonCANCELposY = buttonOKposY;
						final Text textCANCEL = new Text(buttonCANCELposX-50, buttonCANCELposY+4, "Quit completely");
						textCANCEL.setFont(Font.font(java.awt.Font.SANS_SERIF, 15));
						textCANCEL.setFill(Color.FLORALWHITE);						
						final Ellipse buttonCancel = new Ellipse(buttonCANCELposX, buttonCANCELposY, 80, 15);
						buttonCancel.setFill(Color.SLATEBLUE);
						buttonCancel.setStroke(Color.BURLYWOOD);
						buttonCancel.setStrokeWidth(2.0);
						dialogBox.getChildren().add(buttonCancel);
						dialogBox.getChildren().add(textCANCEL);

//						EventHandler<MouseEvent> mouseOKtoReset = new EventHandler<MouseEvent>() {
//							@Override
//							public void handle(MouseEvent event) {
//								// TODO Auto-generated method stub
//								
//							}
//						};
						
						dialog.show();

					}
				}
			};
			infoNewGame.setOnMouseClicked(mouseResetGame);


			final Duration oneGameFrame = Duration.millis(1000/2); // TODO: accelerate later...
			EventHandler<ActionEvent> gameLoopHandler = new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent t) {
					System.err.print("." + count);
					infoNewGame.setText("count = " + count);
					count++;
					if (count >= 8) {
						count = 0;
					}
				}
			};
			final KeyFrame gameLoopControl = new KeyFrame(oneGameFrame, gameLoopHandler);
			final Timeline gameLoop = new Timeline();
			gameLoop.getKeyFrames().add(gameLoopControl);
			gameLoop.setCycleCount(Animation.INDEFINITE);
			Logger.getGlobal().info("prior to gameLoop.play()");
			gameLoop.play();
			Logger.getGlobal().info("\nafter gameLoop.play()");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		Logger.getGlobal().info("main(): before launch()");
		launch(args);
		Logger.getGlobal().info("main(): can clean up after launch()");
	}
}
