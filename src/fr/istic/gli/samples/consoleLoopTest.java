/**
 * 
 */
package fr.istic.gli.samples;

import java.io.Console;

/**
 * @author Jurgen
 *
 */
public class consoleLoopTest {

	public static void main(String[] args) {
		final String promptSpacer = "> ";
		String prompt = "Enter" + promptSpacer;

		Console console;
		if ((console = System.console()) != null) {
			String command;
			while (!(command = console.readLine(prompt)).startsWith("exit")) {
				System.err.println(command);				
			}
		}
	}

}
