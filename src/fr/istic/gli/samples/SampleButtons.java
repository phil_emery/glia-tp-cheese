package fr.istic.gli.samples;
	
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;


/**
 * 
 * Testing animation: numbers moving
 * 
 */
public class SampleButtons extends Application {

    @Override
	public void start(Stage primaryStage) {
		
		try {
			primaryStage.setTitle("Testing buttons");
			
			StackPane root = new StackPane();
			int paneDim = 400;
			Scene scene = new Scene(root, paneDim, paneDim, Color.DARKORANGE);
			
			Button button = new Button("New game?");
			button.setFont(Font.font(java.awt.Font.SANS_SERIF, 20));
			button.setMaxSize(150, 20);
			button.setLayoutX(250);
			button.setLayoutY(50);
//			scene.setFill(Color.DARKORANGE);
//			final Text infoNewGame = new Text(250, 80, "infoText");
//			infoNewGame.setFont(Font.font(java.awt.Font.SANS_SERIF, 14));
//			root.getChildren().add(infoNewGame);

			button.setOnAction(new EventHandler<ActionEvent>() {
				@Override public void handle(ActionEvent t) {
					System.out.println("Starting new game!");
//					infoNewGame.setText("New game!");
				}
			});

			root.getChildren().add(button);

			// CSS currently not used
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			
			
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			
			primaryStage.show();
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
